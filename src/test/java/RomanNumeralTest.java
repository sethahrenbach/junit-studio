import org.junit.Test;

import static org.junit.Assert.*;

public class RomanNumeralTest {

    @Test
    public void FiveIsV() {
        assertEquals("V", RomanNumeral.fromInt(5));
    }

    @Test
    public void FourIsIV() {
        assertEquals("IV", RomanNumeral.fromInt(4));

    }

    @Test
    public void FiftyOneIsLI() {
        assertEquals("LI", RomanNumeral.fromInt(51));

    }
    @Test
    public void NineHundredNinetyNineIsCMXCIX() {
        assertEquals("CMXCIX", RomanNumeral.fromInt(999));

    }
}

