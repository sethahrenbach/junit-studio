import org.junit.Test;

import static org.junit.Assert.*;

public class BalancedBracketsTest {


    @Test
    public void surroundingBalancedBrackets() {
       boolean balanced = BalancedBrackets.hasBalancedBrackets("[LaunchCode]");
       assertTrue(balanced);
    }


    @Test
    public void internalBalancedBrackets() {
        boolean balanced = BalancedBrackets.hasBalancedBrackets("Launch[Code]");
        assertTrue(balanced);
    }

    @Test
    public void lonelyBalancedBrackets() {
        boolean balanced = BalancedBrackets.hasBalancedBrackets("[]");
        assertTrue(balanced);
    }

    @Test
    public void precedingBalancedBrackets() {
        boolean balanced = BalancedBrackets.hasBalancedBrackets("[]LaunchCode");
        assertTrue(balanced);
    }

    @Test
    public void singleOpenUnbalanced() {
        boolean unbalanced = BalancedBrackets.hasBalancedBrackets("[LaunchCode");
        assertFalse(unbalanced);
    }

    @Test
    public void wrongOrderUnbalanced() {
        boolean unbalanced = BalancedBrackets.hasBalancedBrackets("Launch]Code[");
        assertFalse(unbalanced);
    }

    @Test
    public void singleOpenLonelyUnbalanced() {
        boolean unbalanced = BalancedBrackets.hasBalancedBrackets("[");
        assertFalse(unbalanced);
    }

    @Test
    public void wrongOrderLonelyUnbalanced() {
        boolean unbalanced = BalancedBrackets.hasBalancedBrackets("][");
        assertFalse(unbalanced);
    }


}
