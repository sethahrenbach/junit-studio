import org.junit.Test;

import static org.junit.Assert.*;

public class FractionTest {

    @Test
    public void addOneThirdToOneFourthEqualsSevenTwelfths() {

        Fraction fraction1 = new Fraction(1, 3);
        Fraction fraction2 = new Fraction(1, 4);

        Fraction sum = fraction1.add(fraction2);
        Fraction expected = new Fraction(7, 12);
        assertEquals(expected.getDenominator(), sum.getDenominator());
        assertEquals(expected.getNumerator(), sum.getNumerator());
    }

    @Test
    public void addOneThirdToFour() {
        Fraction fraction = new Fraction(1, 4);
        Integer integer = new Integer(4);
        Fraction sum = fraction.add(integer);
        Fraction expected = new Fraction(17, 4);
        assertEquals(expected.getDenominator(), sum.getDenominator());
        assertEquals(expected.getNumerator(), sum.getNumerator());
    }

    @Test( expected = IllegalArgumentException.class)
    public void addOneZeroToOneFourth() {
        Fraction fraction1 = new Fraction(1, 0);
        Fraction fraction2 = new Fraction(1, 4);

        Fraction sum = fraction1.add(fraction2);
        Fraction expected = new Fraction(7, 12);
        assertEquals(expected.getDenominator(), sum.getDenominator());
        assertEquals(expected.getNumerator(), sum.getNumerator());
    }

    @Test
    public void isReducedThreeEighthsPlusOneEighths() {
        Fraction fraction1 = new Fraction(3, 8);
        Fraction fraction2 = new Fraction(1, 8);

        Fraction sum = fraction1.add(fraction2);
        Fraction expected = new Fraction(1, 2);
        assertEquals(expected.getDenominator(), sum.getDenominator());
        assertEquals(expected.getNumerator(), sum.getNumerator());
    }
}
