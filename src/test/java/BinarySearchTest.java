import org.junit.Test;

import static org.junit.Assert.*;

public class BinarySearchTest {

    @Test
    public void findsIndexInSortedArrayWithNumber() {

        int[] sortedInts = {1,2,3,4,5,6,7,8};
        int index = BinarySearch.binarySearch(sortedInts, 4);
        assertEquals(3, index);
    }

    @Test
    public void failsInSortedArrayWithoutNumberHigh() {

        int[] sortedInts = {1,2,3,4,5,6,7,8};
        int index = BinarySearch.binarySearch(sortedInts, 9);
        assertEquals(-1, index);
    }

    @Test
    public void failsInSortedArrayWithoutNumberLow() {

        int[] sortedInts = {1,2,3,4,5,6,7,8};
        int index = BinarySearch.binarySearch(sortedInts, 0);
        assertEquals(-1, index);
    }

    @Test
    public void returnsFailureOnEmptyArray() {
        int[] emptyInts = {};
        int index = BinarySearch.binarySearch(emptyInts, 1);
        assertEquals(-1, index);
    }
}
